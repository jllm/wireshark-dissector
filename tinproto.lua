p_tin_proto = Proto("TINproto","TIN agent")
local f_source = ProtoField.string("TINproto.sourcd", "source")
local f_id_msg = ProtoField.string("TINproto.id_msg", "id_msg")
local f_signature = ProtoField.string("TINproto.signature", "signature")

local f_table = ProtoField.string("TINproto.table", "table")
local f_operation = ProtoField.string("TINproto.operation", "operation")
local f_chain = ProtoField.string("TINproto.chain", "chain")
local f_parameters = ProtoField.string("TINproto.parameters", "parameters")

local f_status = ProtoField.string("TINproto.status", "status")
local f_output = ProtoField.string("TINproto.output", "output")

p_tin_proto.fields = {f_source, f_id_msg, f_signature, f_table, f_operation, f_chain, f_parameters, f_status, f_output}
 
function p_tin_proto.dissector (buf, pkt, root)
  -- validate packet length is adequate, otherwise quit
   if buf:len() == 0 then return end

   str = buf(0):string()

   subtree = root:add(p_tin_proto, buf(0))
   if str:sub(1,1) == 'A' then  --od serwera do agenta
      id_msg_end = string.find(str, "#")
      table_end = string.find(str, "#", id_msg_end + 1)
      operation_end = string.find(str, "#", table_end + 1)
      chain_end = string.find(str, "#", operation_end + 1)
      parameters_end = string.find(str, "#", chain_end + 1)
      signature_end = string.find(str, "$")

      subtree:add(f_source, buf(0, 1))
      subtree:add(f_id_msg, buf(1,id_msg_end - 2))
      subtree:add(f_table, buf(id_msg_end, table_end - id_msg_end - 1))
      subtree:add(f_operation, buf(table_end, operation_end - table_end - 1))
      subtree:add(f_chain, buf(operation_end, chain_end - operation_end - 1))
      subtree:add(f_parameters, buf(chain_end, parameters_end - chain_end - 1))
      subtree:add(f_signature, buf(parameters_end, signature_end - parameters_end - 2))
   elseif str:sub(1,1) == 'C' then
      id_msg_end = string.find(str, "#")
      status_end = string.find(str, "#", id_msg_end + 1)
      output_end = string.find(str, "#", status_end + 1)
      signature_end = string.find(str, "$")

      subtree:add(f_source, buf(0, 1))
      subtree:add(f_id_msg, buf(1,id_msg_end - 2))
      subtree:add(f_status, buf(id_msg_end, status_end - id_msg_end - 1))
      subtree:add(f_output, buf(status_end, output_end - status_end - 1))
      subtree:add(f_signature, buf(output_end, signature_end - output_end - 2))
   end
end

function p_tin_proto.init()
end

local tcp_dissector_table = DissectorTable.get("tcp.port")
dissector = tcp_dissector_table:get_dissector(8002)

tcp_dissector_table:add(8002, p_tin_proto)
